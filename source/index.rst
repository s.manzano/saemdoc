========
Contenu
========

Bienvenue sur la documentation du projet SAEM! Ces documents sont organisés au sein de différents guides qui s'adressent à différents publics

.. image:: images/2050.jpg

.. toctree::
    :maxdepth: 2
    :numbered:
    :titlesonly:
    :glob:
    :hidden:

  guide-utilisateur/index.rst
  guide-utilisateur/ged-sas.rst
  guide-utilisateur/asalae.rst
  guide-utilisateur/referentiel.rst
  guide-installation/index.rst

.. note::

Cette documentation est maintenue par l'équipe projet SAEM. Les modules SAEM et la documentation sont gratuites et open source et les contributions sont les bienvenues.
Pour contribuer à cette documentation, rendez-vous sur :ref:`/contribution.md`.


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

