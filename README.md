Une merveilleuse documentation produite avec des outils libres
## Introduction
Le projet SAEM est un ensemble de modules open-source permettant de mettre en oeuvre les processus d'archivage électronique. 
Il est composé actuellement d'un module de gestion de données de référence, d'un module de pré-versement et d'un module de conservation pérenne.

# Architecture
Les modules de pré-versement et de conservation obtiennent des données en se synchronisant au module de gestion de données de référence à partir de requête HTTP effectuées au moyen du protocole OIA-PMH
# Installation